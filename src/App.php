<?php

namespace Erc\CodeIgniter;

class App {
	public static $CI = null;

	public static function app() {
		if (static::$CI === null) {
			static::$CI =& get_instance();
		}
		return static::$CI;
	}

	/**
	 * Выбор базы данных
	 * @param string $name Наименование базы данных
	 * @return
	 */
	public static function db($name = null) {
		if (isset(static::app()->db_list[$name])) {
			return static::app()->db_list[$name];
		}

		static::app()->db_list[$name] = ($name) ? static::app()->load->database($name, TRUE, TRUE) : self::app()->db;

		return static::app()->db_list[$name];
	}
}
